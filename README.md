## No.1
**Use Case Table**

| No      | Actor      | Dapat  | Prioritas |
| ------  | ------     | ------ | ------ |
| 1 |User        |menonton video berupa movie atau series| 90 |
| 2 |User        |melihat daftar movie dan series yang tersedia| 80 |
| 3 |User        |memilih untuk menonton movie atau series tertentu| 85 |
| 4 |User        |mencari movie atau series tertentu| 75 |
| 5 |User        |mendaftar akun baru dengan memasukkan email dan password| 70 |
| 6 |User        |login ke dalam akun yang telah didaftarkan sebelumnya|75 |
| 7 |User        |mengubah kualitas video| 60 |
| 8 |User        |melihat dan memilih subtitle| 65 |
| 9 |User        |mendapatkan rekomendasi video (movie atau series)| 80 | 
| 10 |User        |melihat list movie atau series berdasarkan kategori| 70 |
| 11 |User        |melihat informasi dari movie atau series yang akan ditonton| 75 |
| 12 |User        |membagikan link dari sebuah movie atau series| 60 |
| 13 |User        |menonton trailer dari movie atau series tertentu sebelum memutarnya| 55 |
| 14 |User        |menambahkan video ke dalam bookmark list| 50 |
| 15 |User        |melihat bookmark list| 40 |
| 16 |User        |menghapus video dari dalam bookmark list| 45 |
| 17 |User        |subscribe paket premium| 80 |
| 18 |User        |User dapat memilih paket langganan (subscribe) premium| 75 |
| 19 |User        |tukar kode voucher| 65 |
| 20 |User        |User dapat memilih metode pembayaran untuk subscribe premium| 70 |
| 21 |User        |mengubah bahasa aplikasi| 55 |
| 22 |User        |menerima notifikasi dari VIU| 60 |
| 23 |User        |menautkan akun VIU di televisi| 70 |
| 24 |User        |melihat status akun pengguna| 50 |
| 25 |User        |melihat FAQ (pertanyaan yang sering ditanyakan)| 45 |
| 26 |User        |melihat jawaban-jawaban mengenai pertanyaan yang sering ditanyakan (FAQ)| 40 |
| 27 |User        |menghubungi VIU| 55 |
| 28 |User        |melakukan pengaduan| 60 |
| 29 |User        |User dapat mengunduh video| 75 |
| 30 |User        |User dapat melihat list video yang diunduh| 65 |
| 31 |User        |User dapat menonton video yang sudah diunduh secara offline| 70 |
| 32 |User        |logout dari akun VIU| 70 |
| 33 |Manajeman(Admin)|mengunggah konten baru beserta informasi konten| 90 |
| 34 |Manajeman(Admin)|mengedit informasi konten yang sudah ada| 85 |
| 35 |Manajeman(Admin)|menghapus konten yang tidak relevan| 80 |
| 36 |Manajeman(Admin)|mengatur pengaturan regional untuk membatasi akses konten tertentu hanya untuk wilayah tertentu| 85 |
| 37 |Manajeman(Admin)|mengelola informasi user, seperti data pribadi, preferensi, atau riwayat tontonan| 80 |
| 38 |Manajeman(Admin)|menghapus atau menangani akun user yang melanggar kebijakan|75|
| 39 |Manajeman(Admin)|menerima dan menjawab keluhan atau pertanyaan user| 70 |
| 40 |Manajeman(Admin)|mengakses laporan dan analitik untuk memantau kinerja aplikasi Viu, seperti statistik penonton, durasi tontonan, atau tingkat interaksi pengguna| 75 |
| 41 |Manajeman(Admin)|mengirim notifikasi kepada user tentang konten baru, pembaruan, atau penawaran khusus| 70 |
| 42 |Manajeman(Admin)|mengatur dan mengonfigurasi pengaturan aplikasi Viu, seperti bahasa, preferensi tampilan, opsi suara, atau pengaturan keamanan| 65 |
| 43 |Manajeman(Admin)|mengelola kategori dan genre konten yang tersedia di aplikasi Viu| 75 |
| 44 |Manajeman(Admin)|menambahkan kategori baru, menghapus kategori yang tidak relevan, atau memperbarui informasi terkait kategori dan genre| 75 |
| 45 |Manajeman(Admin)|memperbarui aplikasi Viu dengan fitur baru, perbaikan bug, atau peningkatan performa| 80 |
| 46 |Direksi Perusahaan|melihat data statistik pengguna seperti jumlah user aktif, penonton per movie/series, dll| 80 |
| 47 |Direksi Perusahaan|dapat menganalisis tren dan pola perilaku user| 85 |
| 48 |Direksi Perusahaan|menghasilkan laporan dan dashboard yang merangkum data bisnis penting| 95 |
| 49 |Direksi Perusahaan|memantau kinerja server dan jaringan untuk memastikan ketersediaan dan kualitas layanan yang baik| 90 |


## No.2
Berikut adalah class diagramnya:

[Class Diagram](ClassDiagram.jpg)

## No.3

[Penerapan Setiap Poin dari SOLID Design Principle](Penerapan_Setiap_Poin_dari_SOLID_Principle.pdf)

## No.4

[Design Pattern yang dipilih](Design_Pattern_yang_Dipilih.pdf)

## No.5

[Konetivitas ke database](Menunjukkan_dan_Menjelaskan_konektivitas_ke_database.pdf)

## No.6

[Web Service dan Operasi CRUD](Web_Service_dan_Operasi_CRUD.pdf)

## No.7

[GUI dari Produk Digital](Menunjukkan_dan_menjelaskan_GUI_dari_produk.pdf)

## No.8

[Penjelasan HTTP connection melalui GUI Produk Digital](Menunjukkan_dan_menjelaskan_HTTP_connection_melalui_GUI_produk.pdf)

## No.9

[Video Penjelasan Produk Digital](https://youtu.be/P3kZSIoU3KQ)
